
.PHONY: all clean

all: evolution.pdf html

# --shell-escape is needed for gnuplottex

evolution.pdf: evolution.tex
	@pdflatex --shell-escape evolution
	@pdflatex --shell-escape evolution

# Both
# (A) second and third arguments to 'htlatex' and
# (B) contents of 'nma.cfg'
# were obtained from 'https://www.12000.org/my_notes/faq/LATEX/htse54.htm'.
html: evolution.pdf
	@mkdir -p html/evolution
	@rm -fv html/evolution/*
	@cp -v evolution.tex nma.cfg html/evolution
	@(cd html/evolution;\
	  htlatex\
	    evolution\
	    "nma.cfg,html,charset=utf-8"\
	    " -cunihtf -utf8"\
	    ""\
	    --shell-escape)

clean:
	@rm -frv html
	@rm -fv *gnuplot*
	@rm -fv *.out
	@rm -fv evolution.aux
	@rm -fv evolution.log
	@rm -fv evolution.pdf

