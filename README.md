

# Modern Science and Aristotelian Metaphysics

My intention is for the present project to hold some ideas related to modern
science in light of Aquinas.

The first and, so far, only document in the repository here is on evolution.


## Why Aristotle

Because Thomas Aquinas is
- a doctor of the Church,
- the greatest classical proponent of natural theology, and
- a great philosopher of ethics (as he identified the principle of double
  effect),

his choice of Aristotelian metaphysics as the basis for his thought ought to
give Catholics a healthy respect for Aristotle.  At the very least, Catholics
living after the time of Aquinas should, because of his fruitfulness and
orthodoxy, regard with at least some skepticism any philosophy opposed to
Aristotelian metaphysics.


## General Relationship Between Modern Science and Metaphysics

Aristotelian metaphysics is consistent with modern science.
- Aristotelian *metaphysics* is consistent, as I argue below, with all
  scientific theories (even ones that have been ruled out by experiment), and
- Aristotelian metaphysics is not Aristotelian *physics* (which has been ruled
  out by experiment!)
- The Aristotelian can accept both
    - the scientific facts in the historical record (the observable outcome of
      every repeatable scientific experiment) and
    - the identification of each standard theory (such as quantum mechanics) as
      the best explanation yet devised for the facts in terms of natural
      causes.

However, Aristotelian metaphysics opposes many a philosophical claim made in
the name of science.

For example, the Aristotelian
- affirms that the best scientific theory of a living thing is that the living
  thing is an arrangement of chemicals but
- denies the philosophical assertion that the living matter in an oak tree is
  *actually* an arrangement of chemicals.

(Living matter cannot actually be an arrangement of chemicals because living
matter is a *substance*.)

Thus Aristotelian metaphysics is not opposed to science, even though
Aristotelian metaphysics can be opposed to a popular philosophical
*interpretation* of a scientific theory.

- Aristotelian metaphysics is consistent with the identification of *any*
  scientific theory as the best current theory for explaining the observational
  facts.
    - The scientific community has the natural authority to decide, at each
      moment in history, which scientific theory best explains all of the
      observational facts.
    - Even if a scientific theory, taken naively, assert what is contradictory
      to Aristotelian metaphysics, the Aristotelian ought not, merely on the
      basis of the contradiction, oppose the legitimacy of the theory.
- Yet even while, on the basis the scientific community's natural authority,
  the Aristotelian accepts that a given theory is the best current theory, the
  Aristotelian might deny that the theory is a true description of nature.
    - The scientific community has no natural authority to decide that one or
      another theory is a true description of nature.
    - The best current description is not necessarily the true description.
    - A future observation might rule the theory out, even if, at the present
      moment in history, the theory be the best one yet advanced to explain the
      observational facts collected so far.

So Aristotelian metaphysics is consistent even with a scientific theory that
appears to contradict the metaphysics, precisely because to claim that the
theory is a true description of nature is to make a philosophical, and not a
scientific, claim.


## Documents


### Evolution

- Versions of document
    - [PDF](https://tevaughan.gitlab.io/modern-science-and-aristotelian-metaphysics/evolution.pdf)
    - [HTML](https://tevaughan.gitlab.io/modern-science-and-aristotelian-metaphysics/html/evolution/evolution.html)
- Synopsis
    - Definition of terms
        - All of reality is divided into what exists potentially and what
          exists actually.
          - Form is the principle of what a thing actually is.
          - Matter is the principle of what a material thing potentially is (if
            the matter be re-formed).
          - Change is the actualization of what had been merely potential.
        - A material substance is a composite of prime matter and substantial
          form.
        - A material thing is a composite of material substance and accidental
          form.
        - A material thing comes into being by way of efficient and final
          causes.
            - In an accidental change, one or more existing substances can be
              recombined into a new accidental form.
            - One substance can have the potential to become another substance,
              and so substantial change can occur, too.
            - An efficient cause, like a match that, when struck, heats ice
              into liquid water, is the agent of change.
            - A final cause is the pointing of an efficient cause toward its
              usual effect.
                - A final cause can be merely physical.
                    - For example, the final cause of the flame produced by a
                      match when it is struck is just that a match, when
                      struck, tends to produce flame.
                    - That striking match can reliably cause the initiation of
                      a flame is an example of a rudimentary final cause.
                - A final cause can be an idea in a mind.
                    - For example, a man who builds a book-case is an efficient
                      cause of the book-case.  (The tools that he uses are also
                      efficient causes, of an instrumental kind.)
                    - The idea of the book-case exists in his mind, before he
                      builds it, as the final cause of the book-case.
                    - A good craftsman reliably builds what he has an idea of,
                      and so the idea counts as a final cause of what is built,
                      just as the craftsman counts as the efficient cause.
        - The living part of an individual organism (not including what is
          about to be excreted, what has recently been ingested, and extruded
          dead material, like hair) is a single substance.
        - The substantial form of a living individual (vegetable, animal, or
          rational) is called "the soul."
        - In metaphysical terms, the highest-level categories (analogous to
          domains or super-kingdoms in biology) are
            - non-living substance, such as a rock,
            - vegetative (or nutritive) substance, such as a pine tree,
            - animal (or sentient) substance, such as a cat, and
            - rational substance, such as a human person.
        - Each higher kind of substance has properties that are irreducible to
          the properties of a lower kind.
            - For example, an animal has sensory experiences (such as seeing
              the color blue) and a corresponding interior life that a plant
              does not have.
            - What makes a higher kind be higher is that it has all of the
              powers of the lower plus powers that the lower does not have.
    - Principal ideas
        - Evolution over time from one species to another within the same
          fundamental metaphysical category presents no real problem.
            - For example, evolution from a reptile to a bird, both animals.
              Because every animal has the same fundamental powers, it is easy
              to see how the principle of proportionate causality is obeyed.
        - Aquinas thought that, in some circumstances, substances of one
          fundamental kind could combine to form a substance of a higher kind.
            - For example, he thought that flies were spontaneously generated
              from non-living, putrefying matter.
            - One might regard Aquinas' view, in light of modern scientific
              theory, as hard now to maintain, so that, considering
              evolutionary theory, one should be skeptical of transformation of
              plant-like organisms into an animal.
            - Yet such evolution could be possible.
                - In order for a substance of a higher category to be formed
                  from substances of a lower category, then the powers of the
                  higher substance must exist potentially (though not actually)
                  in the lower substances.
        - But natural causation cannot explain the emergence of a *rational*
          substance from non-rational substances.
            - This is one of the fundamental assertions of Humani Generis.
            - The human *body* might have, in a sense, resulted from natural
              evolution, but not the human *soul*.
                - According to metaphysical principles, at least *the first*
                  human soul must have appeared by special divine action,
                  perhaps considered as a divine cause that participated along
                  with natural causes at the moment of conception.
                - Or it might be that *every* human soul can appear in the
                  world only by way of such special divine action.
            - One reason for this is that all of the powers of vegetative and
              sentient life are inherently material powers, but the powers
              unique to rational life are *immaterial*.
                - For example, imagination is a material power of an animal;
                  the imagination is a power of the brain.  A cat, for example,
                  has an imagination.
                - But triangularity, which is shared in common by every
                  particular imagined picture of a triangle, is immaterial, and
                  the power to conceive of abstract triangularity is
                  immaterial.
                - Such a power is not a power of the brain, though in humans
                  this power is fully present only when the brain is
                  functioning.
            - Immaterial powers cannot reside, not even potentially, in a
              substance of a purely material kind.

